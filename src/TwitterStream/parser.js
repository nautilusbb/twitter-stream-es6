'use strict';
//
//  Parser - for Twitter Streaming API
//
const EventEmitter = require('events').EventEmitter;

class Parser extends EventEmitter {

    constructor() {
        super();
        this.msg = '';
        EventEmitter.call(this);
    }
    parse(chunk) {
        this.msg += chunk;
        chunk = this.msg;

        const size = chunk.length;
        let start = 0;
        let offset = 0;
        let curr = '';
        let next = '';

        while(offset < size) {
            curr = chunk[offset];
            next = chunk[offset + 1];

            if(curr === '\r' && next === '\n') {
                const piece = chunk.slice(start, offset);
                start = offset += 2;

                if(!piece.length) { continue; } //empty object

                if(piece === 'Exceeded connection limit for user') {
                    this.emit('connection-limit exceeded',
                        new Error(`Twitter API error ${piece} . 
                        Only instantiate one stream per set of credentials.`));
                    continue;
                }
                let msg  = '';
                try {
                    msg = JSON.parse(piece);
                    if(!msg.id_str){
                        msg.id_str = JSON.stringify(msg.id);
                    } else if(!msg.user.id_str){
                        msg.user.id_str = JSON.stringify(msg.user.id);
                    }


                } catch(err) {
                    this.emit('error', new Error(`Error Parsing reply ${piece} 1 stream per credentials `
                    ));
                } finally{
                    if(msg)
                        this.emit('element', msg);
                    continue;
                }
            }
            offset ++;
        }
        this.msg = chunk.slice(start, size);
    }

}
module.exports = Parser;
