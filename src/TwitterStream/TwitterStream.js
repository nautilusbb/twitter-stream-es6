'use strict';
//
//  Twitter API Wrapper
//
const util = require('util');
const endpoints = require('./endpoints');
const queryString = require('querystring');

const StreamingConnector = require('./StreamingConnector');
const REQUIRED_FOR_APP_AUTH = [
    'consumer_key',
    'consumer_secret',
];

// config values required for user auth (superset of app-only auth)
const REQUIRED_FOR_USER_AUTH = REQUIRED_FOR_APP_AUTH.concat([
    'access_token_key',
    'access_token',
]);

const streamEndpointMap = {
    user: endpoints.USER_STREAM,
    site: endpoints.SITE_STREAM,
};

//
//  TwitterStream
//
class TwitterStream {

    constructor(config) {
        // this._validateConfigOrThrow(config.twitterStreaming);
        this.config = config;
        this._twitter_time_minus_local_time_ms = 0;
        // console.log(this instanceof StreamingConnector);
    }

    //helper
    makeQueryString(obj) {
        let qs = queryString.stringify(obj);
        qs = qs.replace(/\!/g, '%21')
            .replace(/\'/g, '%27')
            .replace(/\(/g, '%28')
            .replace(/\)/g, '%29')
            .replace(/\*/g, '%2A');
        return qs;
    }
    /**
     * takes 2 arguments, params, path, and call back
     *  combines path
     */
    _buildRequestOptions(method, path, params, callback) {
        if(!params) {
            params = {};
        }

        const paramsClone = JSON.parse(JSON.stringify(params));
        const finalParams = this._normalizeParams(paramsClone);

        delete finalParams.twit_options;

        const requestOptions = {
            headers: {
                Accept: '*/*',
                'User-Agent': 'twitter-stream',
            },
            gzip: true,
            encoding: null,
        };

        if(typeof this.config.timeout_ms !== 'undefined') {
            requestOptions.timeout = this.config.timeout_ms;
        }
        if(path.match(/^https?:\/\//i)) {
            requestOptions.url = path;
        }
        const endpoint = streamEndpointMap[path] || endpoints.PUB_STREAM;
        requestOptions.url = `${endpoint}${path}.json`;


        if(Object.keys(finalParams).length) {
            const qs = this.makeQueryString(finalParams);
            requestOptions.url += `?${qs}`;
        }
        if(!this.config.app_only_auth) {
            const oauthTs = `${Date.now()}${this._twitter_time_minus_local_time_ms}`;

            requestOptions.oauth = {
                consumer_key: this.config.consumer_key,
                consumer_secret: this.config.consumer_secret,
                token: this.config.access_token_key,
                token_secret: this.config.access_token,
                timestamp: Math.floor(oauthTs / 1000).toString(),
            };
            callback(null, requestOptions);
            return;
        }
    }

    /**
     * Creates/starts a connection object that stays connected to Twitter's servers
     * using Twitter's rules.
     *
     * @param  {String} path   Resource path to connect to (eg. "statuses/sample")
     * @param  {Object} params user's params object
     * @return {StreamingAPIConnection}        [description]
     */
    stream(path, params) {
        const twitOptions = (params && params.twit_options) || {};
        const streamer = new StreamingConnector();

        this._buildRequestOptions('POST', path, params, (err, requestOptions) => {
            if(err) {
                const tError = new Error(err.message);
                streamer.emit('error', tError);
                return;
            }
            streamer.requestOptions = requestOptions;
        streamer.twitOptions = twitOptions;

        process.nextTick(() => {
            streamer.start();
    });
    });

        // console.log(this);
        return streamer;
    }

    _normalizeParams(params) {
        let normalized = params;
        if(params && typeof params === 'object') {
            Object.keys(params).forEach(key => {
                const value = params[key];
            if(Array.isArray(value)) {
                normalized[key] = value.join(',');
            }
        });
        } else if(!params) {
            normalized = {};
        }
        return normalized;
    }

    _validateConfigOrThrow(config) {
        //check config for proper format
        const oType = typeof config;
        if(typeof config !== 'object') {
            throw new TypeError(`config must be object, got ${oType}`);
        }

        if(typeof config.timeout_ms !== 'undefined' && isNaN(Number(config.timeout_ms))) {
            throw new TypeError
            (`Twit config ${config.timeout_ms} must be a Number. Got: ${config.timeout_ms}.`);
        }

        const authType = 'user auth';
        const  requiredKeys = REQUIRED_FOR_USER_AUTH;

        requiredKeys.forEach(reqKey => {
            if(!config[reqKey]) {
            const errMsg = util.format('Twit config must include `%s` when using %s.'
                , reqKey, authType);
            throw new Error(errMsg);
        }
    });
    }

}

module.exports = TwitterStream;
