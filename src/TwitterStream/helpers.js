
class Helpers {

    attachBodyInfoToError(err, body) {
        err.twitterReply =  body;
        if(!body) {
            return;
        } else if(body.error) {
            // body is error object
            err.message = body.error;
            err.allErrors = err.allErrors.concat([body]);
        } else if(body.errors && body.errors.length) {
            //body contains multiple errors
            err.message = body.errors[0].message;
            err.code = body.errors[0].code;
            err.allErrors = err.allErrors.concat(body.errors);
        }
    }

}

module.exports = Helpers;
