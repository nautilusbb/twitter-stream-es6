'use strict';

const EventEmitter = require('events').EventEmitter;
const Parser = require('./parser');
const request = require('request');
const zlib = require('zlib');
const Helpers = require('./helpers');
const STATUS_CODES_TO_ABORT_ON = require('./settings').STATUS_CODES_TO_ABORT_ON;
const helper = new Helpers();

class StreamingConnector extends EventEmitter {

    /**
     * constructor function accepts two mandatory configuration objects
     * to init the streaming connector
     * @constructor
     * @param {Object} requestOptions
     *
     * calls on EventEmitter
     */
    constructor(requestOptions) {
        super();
        this.requestOptions = requestOptions;
        this._twitter_time_minus_local_time_ms = 0;
        EventEmitter.call(this);
    }

    /**
     * Resets the connection.
     * - clears request, response, parser
     * - removes scheduled reconnect handle (if one was scheduled)
     * - stops the stall abort timeout handle (if one was scheduled)
     */
    _resetConnection() {
        //remove all event listeners from EventEmitter obj, then destroy the request || response
        if(this.request) {
            this.request.removeAllListeners();
            this.request.destroy();
        }
        if(this.response) {
            this.response.removeAllListeners();
            this.response.destroy();
        }
        if(this.parser) {
            this.parser.removeAllListeners();
        }
        // ensure a scheduled reconnect does not occur (if one was scheduled)
        // this can happen if we get a close event before .stop() is called
        clearTimeout(this._isScheduledReconnect);
        delete this._isScheduledReconnect;

        // clear our stall abort timeout
        this._stopStallAbortTimeout();
    }
    /**
     * Resets the parameters used in determining the next reconnect time
     */
    _resetReconnectParams() {
        this._connectInterval = 0;
        this._isInstantReconnect = false;
    }
    /**
     * start a first time connection, and setup an event emitting parser that handles
     * incoming twitter messages
     *
     */
    _startPersistentConnection() {
        //bootstrap connection
        const parser = new Parser();
        this._resetConnection();
        this._setupParser(parser);
        this._resetStallAbortTimeout();
        this._setOauthTimestamp();

        //should be changed into promise
        //send request
        this.request = request.post(this.requestOptions);
        this.emit('connect', this.request);

        this.request.on('response', response => {
            this._updateOauthTimestampOffsetFromResponse(response);
        // reset reconnect attempt flag so next attempt go with 0 delay
        // if app gets a tcp error
        this._isInstantReconnect = false;
        this._resetStallAbortTimeout();
        this.response = response;
        if(STATUS_CODES_TO_ABORT_ON.indexOf(this.response.statusCode) !== -1) {
            // received a status code to abort connection
            // read the body from the response and return an error
            let body = '';

            const gunzip = zlib.createGunzip();
            this.response.pipe(gunzip);
            try {
                gunzip.on('data', chunk => {
                    body += chunk.toString('utf8');
            });
            } catch(err) {
                console.log(err.message);
                err;
            }

            gunzip.on('end', () => {
                try {
                    body = JSON.parse(body);
        } catch(jsonDecodeError) {
                const tError = new Errors.ZlibError;
                gunzip.emit('parse-error', tError);
            }
            const streamEndError = new Errors.BadRequestError(`bad twitter streaming request`);
            streamEndError.statusCode = response ? response.statusCode : null;

            //fixme usage of attach body

            helper.attachBodyInfoToError(streamEndError, body);

            this.emit('error', streamEndError);
            //stop the stream
            this.stop();
            body = null;
        });
            gunzip.on('error', err => {
                //if twitter returns an uncompressed http res, gzip will error
                //handle this by emitting an error with the uncompressed res body
                console.log(err.message);
            const errMsg = `Gzip error: ${err.message} `;
            const uncompressedChunkError = new Errors.ZlibError(errMsg);
            uncompressedChunkError.statusCode = this.response.statusCode;
            helper.attachBodyInfoToError(uncompressedChunkError, this.body);
            this.emit('parse_error', uncompressedChunkError);
        });
        } else if(this.response.statusCode === 420) {
            // console.log(`Rate Limit ${this.response.statusMessage}
            // ${this.response.statusCode}`);
            // const rlErrMsg = this.response.statusMessage;
            // const rateLimitError = helper.makeTwitError(rlErrMsg);
            // rateLimitError.statusCode = this.response.statusCode;
            // helper.attachBodyInfoToError(rateLimitError, this.body);
            const tError = new Errors.RateLimitError(this.response.statusMessage);
            this.emit('rate_limit', tError);
            //force close the connection, which inits  a scheduled 'this.onClose()
            this._scheduleReconnect();
        } else {
            // Received  an OK status code - the response should be valid
            //read the body from response and return it
            const gunzip = zlib.createGunzip();
            this.response.pipe(gunzip);

            //pass response data to parser
            gunzip.on('data', chunk => {
                this._connectInterval = 0;
            // stop stall timer, an dstar a new one
            this._resetStallAbortTimeout();
            parser.parse(chunk.toString('utf8'));
        });
            gunzip.on('close', this._onClose.bind(this));
            gunzip.on('error', err => {
                this.emit('error', new Errors.ZlibError(err.message));
        });
            this.response.on('error', err => {
                this.emit('error', new Errors.NotFoundError(err.message));
        });

            // connected without an error response from Twitter, emit `connected` event
            // this must be emitted after all its event handlers are bound
            // so the reference to `self.response` is not interfered-with by
            //the user until it is emitted
            this.emit('connected', this.response);
        }
    });
        this.request.on('close', this._onClose.bind(this));
        this.request.on('error', err => {
            const tError = new Errors.NotFoundError(err.message);
        console.log(`request connection error ${tError}`);
        this._scheduleReconnect.bind(this);
    });
        console.log(this);
        return this;
    }
    /**
     * Handle when the request or response closes.
     * Schedule a reconnect according to Twitter's reconnect guidelines
     *
     */
    _onClose() {
        this._stopStallAbortTimeout();
        if(this._scheduleReconnect()) {
            //dont reschedule a new reconnect if one is already rescheduled
            return;
        }
        this._scheduleReconnect();
    }
    /**
     * public Start persistent connection to streaming API endpoint
     *
     */
    start() {
        this._resetReconnectParams();
        this._startPersistentConnection();
        return this;
    }
    /**
     * Abort Http request, stop scheduled reconnect ( if it exists) and clear state
     *
     */
    stop() {
        this._resetConnection();
        this._resetReconnectParams();
        return this;
    }
    /**
     * Stop and restart the stall abort timer (called when new data is received)
     * If we go 90s without receiving data from twitter, we abort the request & reconnect
     *
     */
    _resetStallAbortTimeout() {
        this._stopStallAbortTimeout();
        this._stallAbortTimeout = setTimeout(() => {
                this._scheduleReconnect();
    }, 90000);
        return this;
    }
    /**
     * Stop Stalling timeout
     */
    _stopStallAbortTimeout() {
        clearTimeout(this._stallAbortTimeout);
        delete this._stallAbortTimeout;
        return this;
    }
    /**
     * Computes the next time a reconnect should occur (based on the last HTTP response received)
     * and starts a timeout handle to begin reconnecting after `self._connectInterval` passes.
     * back off strategy
     * @return {Undefined}
     * emits
     * reconnecting event
     */
    _scheduleReconnect() {
        if(this.response && this.response.statusCode === 420) {
            //app is being rate-limited
            //start with 1 mintue wait and double wait each attempt
            if(!this._connectInterval) {
                this._connectInterval = 60000;
            } else {
                this._connectInterval *= 2;
            }
        } else if(this.response && this.response.statusCode >= 500) {
            //twitter server side error 5xx
            //start with a 5s wait, double each attempt up to 320s
            if(!this._connectInterval) {
                this._connectInterval = 5000;
            } else if(this._connectInterval < 320000) {
                this._connectInterval *= 2;
            } else {
                this._connectInterval = 320000;
            }
        } else {
            //didn't get an http response from last connection attempt
            //TCP error, or a stall in the stream( and stall timer closed connection )
            if(!this._isInstantReconnect) {
                //first reconnection attempt on a valid connection should occur immediately
                this._connectInterval = 0;
                this._isInstantReconnect = true;
            } else if(this._connectInterval < 16000) {
                //linearly increase delay by 250ms up to 16s
                this._connectInterval += 250;
            } else {
                //cap out reconnect interval at 16s
                this._connectInterval = 16000;
            }
        }
        //schedule the reconnect attempt
        this._isScheduledReconnect = setTimeout(() => {
                this._startPersistentConnection();
    }, this._connectInterval);
        this.emit('reconnecting', this.request, this.response, this._connectInterval);
    }

    /**
     * parses chuncked twitter streaming api responses, using an event emitting parser
     * emits :
     * -tweet
     * -disconnected
     * -limit
     * -warning
     * -status_withheld
     * -user_withHeld
     *-empty_hashtags_object
     * -empty_user_object
     */

    _setupParser(parser) {
        //parser deals with twitter objects as they come in
        //emit the generic 'message' event
        //plus the the specific event corresponding to the message
        parser.on('element', msg => {
            this.emit('tweet', msg);

        if(msg.delete) {
            this.emit('delete', msg);
        } else if(msg.disconnect) {
            this._handleDisconnect(msg);
        } else if(msg.limit) {
            this.emit('limit', msg);
        } else if(msg.scrub_geo) {
            this.emit('scrub_geo', msg);
        } else if(msg.warning) {
            this.emit('warning', msg);
        } else if(msg.status_withheld) {
            this.emit('status_withheld', msg);
        } else if(msg.user_withheld) {
            this.emit('user_withheld', msg);
        } else if(msg.user === undefined) {
            this.emit('empty_user_object', msg);
            msg.user = null;
        } else if(msg.entities.hashtags === undefined) {
            this.emit('empty_hashtags_object', msg);
            msg.entities.hashtags = {};
        }
    });
        parser.on('error', err => {
            const tError = new Errors.NotFoundError(err.message);

        this.emit('parsing_error', tError);
    });
        parser.on('connection-limit-exceeded', err => {
            const tError = new Errors.RateLimitError(err.message);
        this.emit('error', tError);
    });
    }
    /**
     * React to disconnect message from Twitter API
     * emits
     * -stream_disconnected
     */
    _handleDisconnect(twitterEvent) {
        this.emit('stream_disconnected', twitterEvent);
        this.stop();
    }
    /**
     * public reset reconnection params, send http request, and keep the connection alive
     *
     */
    _setOauthTimestamp() {
        if(this.requestOptions.oauth.timestamp) {
            const oauthTimestamp = Date.now() + this._twitter_time_minus_local_time_ms;
            this.requestOptions.oauth.timestamp = Math.floor(oauthTimestamp / 1000).toString();
        }
    }
    /**
     * Call whenever an http response is received from Twitter,
     * to set our local timestamp offset from Twitter's server time.
     * This is used to set the Oauth timestamp for our next http request
     * to Twitter (by calling _setOauthTimestamp).
     *
     * @param  {http.IncomingResponse} resp   http response received from Twitter.
     */
    _updateOauthTimestampOffsetFromResponse(response) {
        if(response && response.headers && response.headers.date &&
            new Date(response.headers.date).toString() !== 'Invalid Date') {
            const twitterTimeMs = new Date(response.headers.data).getTime();
            this._twitter_time_minus_local_time_ms = twitterTimeMs - Date.now();
        }
    }

}

module.exports = StreamingConnector;
// module.exports = Parser;
