'use strict';

module.exports = {
    consumer_key: '***',
    consumer_secret: '***',
    access_token_key: '***-***',
    access_token: '***',
    timeout_ms: '60000',
};
