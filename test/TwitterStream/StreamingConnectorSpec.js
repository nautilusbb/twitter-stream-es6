'use strict';


const EventEmitter = require('events').EventEmitter;
const TwitterStream = require('../../../../src//TwitterStream/TwitterStream');
const StreamingConnector = require('../../../../src/TwitterStream/StreamingConnector');
const Helpers = require('../../../../src/TwitterStream/helpers');

const requestOptions = {
    headers:
    {
        Accept: '*/*', 'User-Agent': 'twitter-stream',
    },
    gzip: true,
    encoding: null,
    timeout: '60000',
    url: 'https://stream.twitter.com/1.1/statuses/filter.json?track=donald%20trump%2Chillary%20clinton%2C%23%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A_%D9%85%D8%AE%D9%86%D9%88%D9%82_%D8%B9%D8%B4%D8%A7%D9%86',
    oauth:
    { consumer_key: config.twitterStreaming.consumer_key,
        consumer_secret: config.twitterStreaming.consumer_secret,
        token: config.twitterStreaming.access_token_key,
        token_secret: config.twitterStreaming.access_token,
        timestamp: '14785287833',
    },
};


describe('StreamingConnector', () => {
    let streamConnect;
    let theValidator;
    let twitterStream;
    let helper;

    beforeEach(() => {
        twitterStream = new TwitterStream({
            consumer_key: config.twitterStreaming.consumer_key,
            consumer_secret: config.twitterStreaming.consumer_secret,
            access_token_key: config.twitterStreaming.access_token,
            access_token: config.twitterStreaming.access_token,
            timeout_ms: '60000',
        });
        streamConnect = new StreamingConnector(requestOptions);
        let helper = new Helpers();
        theValidator = new Validator();
    });
    describe('Initialization', () => {
        it('should instantinate a new Streaming Connector', () => {
            expect(streamConnect).to.be.an.instanceof(StreamingConnector);
            expect(streamConnect).to.be.an.instanceof(EventEmitter);
            expect(streamConnect).to.be.an('object');
            expect(streamConnect).to.not.be.undefined;
            expect(streamConnect.start).to.be.a('function');
            expect(streamConnect.stop).to.be.a('function');
        });
    });

    describe('_resetReconnectParams', () => {
        it('should reset _connectInterval and _isInstantReconnect',() => {
            expect(streamConnect._resetReconnectParams).to.be.ok;
            expect(streamConnect._resetReconnectParams).to.not.be.undefined;
            expect(streamConnect._resetReconnectParams).to.be.a('function');


        });
    });

    describe('_onClose', () => {
        it('should schedule a reconnect',() => {
            expect(streamConnect._onClose).to.be.ok;
            expect(streamConnect._onClose).to.not.be.undefined;
            expect(streamConnect._onClose).to.be.a('function');
        });
    });

    describe('_resetStallAbortTimeout', () => {
        it('should create a 90 sec. time out before calling _scheduleReconnect',() => {
            expect(streamConnect._resetStallAbortTimeout).to.be.ok;
            expect(streamConnect._resetStallAbortTimeout).to.not.be.undefined;
            expect(streamConnect._resetStallAbortTimeout).to.be.a('function');
        });
    });

    describe('_stopStallAbortTimeout', () => {
        it('should clear the stall abort timeout',() => {
            expect(streamConnect._stopStallAbortTimeout).to.be.ok;
            expect(streamConnect._stopStallAbortTimeout).to.not.be.undefined;
            expect(streamConnect._stopStallAbortTimeout).to.be.a('function');
        });
    });

    describe('_scheduleReconnect', () => {
        it('should schedule reconnect in 3 cases: TCP connection error, ' +
            'Twitter 5xx error, or Twitter ratelimiting error',() => {
            expect(streamConnect._scheduleReconnect).to.be.ok;
            expect(streamConnect._scheduleReconnect).to.not.be.undefined;
            expect(streamConnect._scheduleReconnect).to.be.a('function');
        });
    });

    describe('_setupParser', () => {
        it('should parse twitter responses as they come in,' +
            ' and emit events based on message content',() => {
            expect(streamConnect._setupParser).to.be.ok;
            expect(streamConnect._setupParser).to.not.be.undefined;
            expect(streamConnect._setupParser).to.be.a('function');
        });
    });

    describe('_handleDisconnect', () => {
        it('should emit event disconnection and stop the stream,' +
            ' when twitter disconnect event is raised',() => {
            expect(streamConnect._handleDisconnect).to.be.ok;
            expect(streamConnect._handleDisconnect).to.not.be.undefined;
            expect(streamConnect._handleDisconnect).to.be.a('function');
        });
    });

    describe('_setOauthTimestamp', () => {
        it('should set timestamp for requestOptions object', () => {
            expect(streamConnect._setOauthTimestamp).to.be.ok;
            expect(streamConnect._setOauthTimestamp).to.not.be.undefined;
            expect(streamConnect._setOauthTimestamp).to.be.a('function');
        });
    });

    describe('_updateOauthTimestampOffsetFromResponse', () => {
        it('should synchronize Oauth timesteamp with response timestamp', () => {
            expect(streamConnect._updateOauthTimestampOffsetFromResponse).to.be.ok;
            expect(streamConnect._updateOauthTimestampOffsetFromResponse).to.not.be.undefined;
            expect(streamConnect._updateOauthTimestampOffsetFromResponse).to.be.a('function');
        });
    });

    describe('_startPersistentConnection', () => {
        it('should bootstrap streaming connection, initialize Parser object,' +
           ' _resetConnection, _resetStallAbortTimeout',() => {
            expect(streamConnect._startPersistentConnection).to.be.ok;
            expect(streamConnect._startPersistentConnection).to.not.be.undefined;
            expect(streamConnect._startPersistentConnection).to.be.a('function');
        });
    });

    describe('_resetConnection', () => {
        it('should reset connection objects, removing all ' +
            'event listeners, clear scheduled reconnect, and reset stall abort timeout' ,() => {
            expect(streamConnect._resetConnection).to.be.ok;
            expect(streamConnect._resetConnection).to.not.be.undefined;
            expect(streamConnect._resetConnection).to.be.a('function');
        });
    });
    describe('start', () => {
        it('should start a persistent streaming connection', () => {
            expect(streamConnect.start).to.be.ok;
            expect(streamConnect.start).to.not.be.undefined;
            expect(streamConnect.start).to.be.a('function');
        });
    });
    // describe('attach body info to error ', () => {
    //     it('should concat response body info into error message & code object', () => {
    //         expect(helper.attachBodyInfoToError).to.be.a('function');
    //     });
    // });
});
