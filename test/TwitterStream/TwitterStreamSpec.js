'use strict';
const sinon = require('sinon');
const util = require('util');

const TwitterStream = require('../../../../src/TwitterStream/TwitterStream');
const StreamingConnector = require('../../../../src/TwitterStream/StreamingConnector');
const TestHelper = require('./testhelper.js');

exports.checkStreamStopState = stream => {
    expect(stream._connectInterval).to.equal(0);
    expect(stream._usedFirstReconnect).to.not.be.true;
    expect(stream._scheduledReconnect).to.be.undefined;
    expect(stream._stallAbortTimeout).to.be.undefined;
};
exports.checkStream =  (stream, done) => {
    stream.on('connected',  () => {
        console.log('\nconnected');
    });

    stream.once('tweet', tweet => {
        stream.stop();
        expect(tweet).to.be.ok;
        expect(tweet.text).to.be.a('string');
        expect(tweet.id_str).to.be.a('string');
        // console.log(`\n tweet ${tweet.text} `);
        done();
    });
    stream.on('reconnecting', (req, res, connectInterval) => {
        console.log('Got disconnected. Scheduling reconnect! statusCode:'
            , res.statusCode, 'connectInterval', connectInterval);
    });
    stream.on('error', err => {
        console.log('Stream emitted an error', err);
        return done(err);
    });
};

describe('TwitterStream', () => {
    let twitterStream;
    let streamConnect;
    const path = 'statuses/filter';
    const params = 'donald trump';
    const qs = { track: 'donald trump, hillary clinton' };

    beforeEach(() => {
        twitterStream = new TwitterStream(config.twitterStreaming);
        streamConnect = new StreamingConnector();
    });
    describe('Initialization', () => {
        it('should instantinate a new TwitterStream', () => {
            expect(twitterStream).to.be.an.instanceof(TwitterStream);
            // expect(twitterStream.config.twitterStreaming).to.be.an('object');
            // expect(twitterStream.config.twitterStreaming).to.not.be.undefined;
            expect(twitterStream.stream).to.be.a('function');
            expect(twitterStream._twitter_time_minus_local_time_ms).to.eql(0);
        });
    });

    describe('makeQueryString', () => {
        it('should take an object and reformat it to be attached to the request url', () => {
            expect(twitterStream.makeQueryString).to.be.ok;
            expect(twitterStream.makeQueryString).to.be.a('function');
            expect(twitterStream.makeQueryString(qs)).to.not.be.undefined;
            expect(twitterStream.makeQueryString(qs)).to.be.a('String');
        });
    });
    describe('_buildRequestOptions', () => {
        it('should build request Object using path, and params, ' +
            'attach requestOptions to request object', () => {
            const cb = function(err, requestOptions) {
                expect(twitterStream._buildRequestOptions('POST', path, params, cb)).to.be.ok;
                expect(twitterStream._buildRequestOptions
                ('POST', path, params, cb).to.be.an('object'));
                expect(twitterStream._buildRequestOptions).to.be.ok;
                expect(twitterStream._buildRequestOptions).to.be.a('function');
                expect(twitterStream._buildRequestOptions).to.not.be.undefined;
            };
        });
    });
    describe('stream', () => {
        it('should initialize twitter stream using path and request params', () => {
            expect(twitterStream.stream).to.be.ok;
            expect(twitterStream.stream).to.be.a('function');
            expect(twitterStream.stream(path, params)).to.not.be.undefined;
            expect(twitterStream.stream(path, params)).to.be.an('object');
            expect(twitterStream.stream(path, params.requestOptions)).to.not.be.undefined;
            expect(twitterStream.stream(path, params).requestOptions).to.be.an('object');
            const ts = twitterStream.stream('statuses/filter',
                            { track: 'npm' });
            ts.on('limit', limitMsg => {
                console.log('limit', limitMsg);
            });
            ts.on('disconnect', disconnMsg => {
                console.log('stream_disconnected', disconnMsg);
            });
            ts.on('reconnecting', (req, res, interval) => {
                console.log('reconnect. statusCode:', res.statusCode, 'interval:', interval);
            });
            ts.on('connect', req => {
                req;
                console.log('connect');
            });
            ts.on('rate_limit', limitMsg => {
                expect(limitMsg).to.not.be.undefined;
            });
            ts.on('stream_disconnected', disconnectMsg => {
                expect(disconnectMsg).to.not.be.undefined;
            });
        });
        it('should check streamreconnection strategy', done => {
            const twitStream = twitterStream.stream('statuses/filter',
                { track: 'npm' });
            exports.checkStream(twitStream, done);
            let started = false;
            let numTweets = 0;
            twitStream.on('tweet', tweet => {
                process.stdout.write('.');
                expect(tweet).to.be.ok;
                expect(tweet.text).to.be.a('String');
                expect(tweet.id_str).to.be.a('String');
                if(!started) {
                    started = true;
                    numTweets++;
                    console.log('received tweet', numTweets);

                    console.log('stopping stream');
                    twitStream.stop();

                    exports.checkStreamStopState(ts);
                    if(numTweets === 2) {
                        done();
                    } else {
                        started = false;
                        console.log('restarting stream');

                        setTimeout(() => {
                            twitStream.start();
                        }, 1000);
                    }
                }
            });
        });
        it('stopping & restarting the stream works', () => {
            const ts = twitterStream.stream('statuses/filter', { track: 'trump' });
            setTimeout(() => {
                ts.stop();
                exports.checkStreamStopState(ts);
                console.log('\nstopped stream');
            }, 2000);
            setTimeout(() => {
                ts.once('connected', req => {
                    req;
                    console.log('\nrestarted stream');
                    ts.stop();
                    exports.checkStreamStopState(ts);
                    console.log('\nstopped stream');
                    // done();
                });
                ts.start();
            }, 3000);
        });
    });
    describe('streaming reconnect', done => {
        it('correctly implements connection closing backoff', done => {
            const stubPost = () => {
                const tHelper = new TestHelper();

                process.nextTick(() => {
                    tHelper.fakeRequest.emit('close');
                });
                return tHelper.fakeRequest;
            };
            // const myStubPost = sinon.stub(request, 'post', stubPost => {
            // myStubPost;
            // });
            const ts = twitterStream.stream('statuses/filter', { track: 'npm' });

            const reconnects = [0, 250, 500, 750];
            let reconnectCount = -1;

            let testDone = false;

            ts.on('reconnect',  () => {
                if(testDone) {
                    return;
                }
                reconnectCount += 1;
                let expectedInterval = reconnects[reconnectCount];
                expectedInterval;
                expect(ts._connectInterval).to.be.equal(expectedInterval);
                ts._startPersistentConnection();

                if(reconnectCount === reconnects.length - 1) {
                    stubPost.restore();
                    testDone = true;
                    return done();
                }
            });
            done();
        });
    });
    describe('_normalizeParams', () => {
        it('should check for all config params, using _validateConfigOrThrow', () => {
            expect(twitterStream._normalizeParams).to.be.ok;
            expect(twitterStream._normalizeParams).to.be.a('function');
            expect(twitterStream._normalizeParams).to.not.be.undefined;
        });
    });
});
