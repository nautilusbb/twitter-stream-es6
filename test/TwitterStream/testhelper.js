const EventEmitter = require('events').EventEmitter;
const stream = require('stream');


class TestHelpers extends EventEmitter {

    constructor() {
        EventEmitter.call(this);
    }
    fakeRequest() {
        this.fakeRequest.prototype.destroy = () => {

        };
    }
    fakeResponse(statusCode, body) {
        if(!body) {
            body  = '';
        }
        this.statusCode = statusCode;
        stream.Readable.call(this);
        this.push(body);
        this.push(null);
    }
    _read() {

    }
    destroy() {

    }

}
module.exports = TestHelpers;
