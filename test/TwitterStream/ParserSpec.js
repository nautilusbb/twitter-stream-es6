'use strict';

const EventEmitter = require('events').EventEmitter;
const StreamingConnector = require('../../../../src//TwitterStream/StreamingConnector');
const Parser = require('../../../../src/TwitterStream/parser');

describe('Parser', () => {
    let streamConnect;
    let parser;
    let chunk = 'my name is';

    beforeEach(() => {
        streamConnect = new StreamingConnector();
        parser = new Parser();
    });

    describe('initialization', () => {
        it('should instantinate a Parser object', () => {
            expect(parser).to.be.ok;
            expect(parser).to.not.be.undefined;
            expect(parser).to.be.an.instanceof(Parser);
            expect(parser).to.be.an.instanceof(EventEmitter);
        });
    });

    describe('parse', () => {
        it('should prase chunks as recieved from twitter', () => {
            expect(parser.parse).to.be.ok;
            expect(parser.parse).to.be.a('function');
        });
    });
});
